package com.noname.domain.usecases

import com.noname.domain.models.Food
import com.noname.domain.models.None
import com.noname.domain.models.Resource
import com.noname.domain.repositories.FoodRepository
import com.noname.domain.usecases.base.UseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.*
import javax.inject.Inject
import javax.inject.Named

class GetFoodUseCase @Inject constructor(
    private val foodRepository: FoodRepository,
    @param:Named("io") val dispatcher: CoroutineDispatcher
) : UseCase<List<Food>, None> {

    override fun run(params: None?): Flow<Resource<List<Food>>> {
        return flow<Resource<List<Food>>> {
            try {
                emit(Resource.Success(foodRepository.getFood()))
            } catch (e: Exception) {
                emit(Resource.Error(e))
            }
        }.onStart {
            emit(Resource.Loading())
        }
            .flowOn(dispatcher)
            .catch { Resource.Error<Unit>(it) }
    }
}
package com.noname.domain.repositories

import com.noname.domain.models.Food

interface FoodRepository {
    suspend fun getFood(): List<Food>
}
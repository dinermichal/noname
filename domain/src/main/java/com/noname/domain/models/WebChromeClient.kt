package com.noname.domain.models

import kotlinx.coroutines.flow.StateFlow

interface WebChromeClient {
    val progressState: StateFlow<Int>
}
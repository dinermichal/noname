package com.noname.domain.models

sealed class Resource<T>(open val data: T? = null) {
    var wasHandled = false
        protected set

    data class Success<T>(override val data: T) : Resource<T>() {

        val unhandledData: T?
            get() = if (wasHandled) {
                null
            } else {
                data
            }.also { wasHandled = true }
    }

    class Error<T>(val error: Throwable) : Resource<T>() {

        val unhandledError: Throwable?
            get() = if (wasHandled) {
                null
            } else {
                error
            }.also { wasHandled = true }
    }

    class Loading<T> : Resource<T>() {

        override fun equals(other: Any?): Boolean {
            return other is Loading<*>
        }

        override fun hashCode(): Int {
            return javaClass.hashCode()
        }
    }
}
package com.noname.domain.models

sealed class WebViewClientState {
    object OnPageStarted : WebViewClientState()
    object OnPageFinished : WebViewClientState()
    class OnReceivedError(val error: CharSequence) : WebViewClientState()
}
package com.noname.domain.models

import kotlinx.coroutines.flow.StateFlow

interface WebViewClient {
    val clientState: StateFlow<WebViewClientState>
}
package com.noname.domain.models

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import java.text.SimpleDateFormat
import java.util.*

@Entity
data class Food(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val content: String,
    val imageUrl: String,
    val modificationDate: String,
    val orderId: Long,
    val title: String
) {
    @Ignore
    val url = content.split("\t")[1]

    @Ignore
    val description = content.split("\t")[0]

    @Ignore
    val dateFormatted =
        SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(modificationDate).toString()
}
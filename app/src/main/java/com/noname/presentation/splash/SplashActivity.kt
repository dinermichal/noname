package com.noname.presentation.splash

import android.os.Bundle
import androidx.constraintlayout.motion.widget.MotionLayout
import com.noname.R
import com.noname.core.ui.BaseActivity
import com.noname.databinding.ActivitySplashScreenBinding
import com.noname.di.splash.SplashInjector
import javax.inject.Inject

class SplashActivity : BaseActivity<ActivitySplashScreenBinding>() {

    private val injector: SplashInjector
        get() = application as SplashInjector

    override val layoutId: Int = R.layout.activity_splash_screen

    @Inject
    lateinit var navigator: SplashNavigator

    override fun onCreate(savedInstanceState: Bundle?) {
        injector.inject(this)
        super.onCreate(savedInstanceState)
        binding.motionLayout.setTransitionListener(object : MotionLayout.TransitionListener {
            override fun onTransitionStarted(p0: MotionLayout?, p1: Int, p2: Int) = Unit

            override fun onTransitionChange(p0: MotionLayout?, p1: Int, p2: Int, p3: Float) = Unit

            override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) =
                navigator.navigateToMain()

            override fun onTransitionTrigger(p0: MotionLayout?, p1: Int, p2: Boolean, p3: Float) =
                Unit

        })
    }

    override fun onDestroy() {
        injector.clearSplashComponent()
        super.onDestroy()
    }
}
package com.noname.presentation.splash

import android.app.Activity
import android.content.Intent
import androidx.navigation.ActivityNavigator
import androidx.navigation.NavOptions
import com.noname.presentation.main.MainActivity
import javax.inject.Inject


class SplashNavigator @Inject constructor(
    private val activity: Activity
) {

    fun navigateToMain() {
        val activityNavigator = ActivityNavigator(activity)
        val destination = activityNavigator.createDestination().setIntent(
            Intent(
                activity,
                MainActivity::class.java
            )
        )
        val navOptions = NavOptions.Builder()
            .setEnterAnim(android.R.anim.fade_in)
            .setExitAnim(android.R.anim.fade_out)
            .build()
        activityNavigator.navigate(destination, null, navOptions, null)
        activityNavigator.popBackStack()
    }
}
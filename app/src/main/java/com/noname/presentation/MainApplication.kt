package com.noname.presentation

import com.noname.BuildConfig
import com.noname.di.InjectorApplication
import timber.log.Timber

class MainApplication : InjectorApplication() {

    override fun onCreate() {
        super.onCreate()
        initTimber()
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
    }
}
package com.noname.presentation.second

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.noname.domain.models.WebChromeClient
import com.noname.domain.models.WebViewClient
import com.noname.domain.models.WebViewClientState
import com.noname.presentation.second.SecondFragment.Companion.PROGRESS_INIT_VALUE
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class SecondViewModel @Inject constructor(
    private val webViewClient: WebViewClient,
    private val webChromeClient: WebChromeClient
) : ViewModel() {

    private val _clientState = MutableLiveData<WebViewClientState>(WebViewClientState.OnPageStarted)
    val clientState: LiveData<WebViewClientState> = _clientState

    private val _progressState = MutableLiveData(PROGRESS_INIT_VALUE)
    val progressState: LiveData<Int> = _progressState

    init {
        viewModelScope.launch {
            webViewClient.clientState.collect {
                _clientState.value = it
            }
        }
        viewModelScope.launch {
            webChromeClient.progressState.collect {
                _progressState.value = it
            }
        }
    }
}
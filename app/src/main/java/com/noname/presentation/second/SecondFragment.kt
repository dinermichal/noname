package com.noname.presentation.second

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.navGraphViewModels
import com.noname.R
import com.noname.core.ui.BaseFragment
import com.noname.databinding.FragmentSecondBinding
import com.noname.di.second.SecondInjector
import com.noname.domain.models.WebChromeClient
import com.noname.domain.models.WebViewClient
import com.noname.domain.models.WebViewClientState
import com.noname.presentation.main.MainViewModel
import javax.inject.Inject
import android.webkit.WebChromeClient as AndroidWebChromeClient
import android.webkit.WebViewClient as AndroidWebViewClient

class SecondFragment : BaseFragment<FragmentSecondBinding>() {

    override val layoutId: Int = R.layout.fragment_second

    private val injector: SecondInjector
        get() = baseActivity.application as SecondInjector

    private val isTablet
        get() = requireContext().resources.getBoolean(R.bool.isTablet)

    @Inject
    lateinit var viewModelProviderFactory: ViewModelProvider.Factory

    @Inject
    lateinit var navigator: SecondNavigator

    @Inject
    lateinit var webChromeClient: WebChromeClient

    @Inject
    lateinit var webViewClient: WebViewClient

    private val secondViewModel: SecondViewModel by viewModels { viewModelProviderFactory }

    private val mainViewModel: MainViewModel by navGraphViewModels(R.id.nav_graph_main) { viewModelProviderFactory }

    companion object {
        const val PROGRESS_INIT_VALUE = 0
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        injector.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.webview.webViewClient = webViewClient as AndroidWebViewClient
        binding.webview.webChromeClient = webChromeClient as AndroidWebChromeClient
        setupLiveDataObservers()

    }

    private fun setupLiveDataObservers() {
        mainViewModel.url.observe(viewLifecycleOwner) {
            binding.webview.loadUrl(it)
        }
        secondViewModel.clientState.observe(viewLifecycleOwner) {
            when (it) {
                is WebViewClientState.OnPageFinished -> {
                    binding.progressBar.visibility = View.GONE

                }
                is WebViewClientState.OnPageStarted -> {
                    binding.progressBar.progress = PROGRESS_INIT_VALUE
                    binding.progressBar.visibility = View.VISIBLE
                    binding.error.visibility = View.GONE
                }
                is WebViewClientState.OnReceivedError -> {
                    if (isTablet) {
                        binding.webview.visibility = View.GONE
                        binding.error.visibility = View.VISIBLE
                        binding.error.text = it.error
                    } else {
                        binding.webview.webChromeClient = null
                        navigator.navigateBack()
                        Toast.makeText(context, it.error, Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
        secondViewModel.progressState.observe(viewLifecycleOwner) {
            binding.progressBar.progress = it
        }
    }

}

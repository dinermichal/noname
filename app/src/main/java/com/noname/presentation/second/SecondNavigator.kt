package com.noname.presentation.second

import androidx.fragment.app.Fragment
import com.noname.core.navigation.BaseFragmentNavigator
import javax.inject.Inject

class SecondNavigator @Inject constructor(
    fragment: Fragment
) : BaseFragmentNavigator(fragment) {

    fun navigateBack() {
        popBackStack()
    }
}
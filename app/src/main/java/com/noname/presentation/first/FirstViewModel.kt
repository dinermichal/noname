package com.noname.presentation.first

import androidx.lifecycle.*
import com.noname.domain.models.Food
import com.noname.domain.models.None
import com.noname.domain.models.Resource
import com.noname.domain.usecases.GetFoodUseCase
import javax.inject.Inject

class FirstViewModel @Inject constructor(
    private val getFood: GetFoodUseCase
) : ViewModel() {
    companion object {
        private const val SUCCESS = 0
        private const val LOADING = 1
        private const val ERROR = 2
    }

    private val _food = MutableLiveData<List<Food>>()
    val food: LiveData<List<Food>> = _food

    val displayedChild = MutableLiveData(LOADING)
    val isLoading: LiveData<Boolean> = Transformations.map(displayedChild) {
        it == LOADING
    }

    init {
        getFoodList()
    }

    fun getFoodList() = getFood(viewModelScope, None) {
        when (it) {
            is Resource.Success -> {
                displayedChild.value = SUCCESS
                _food.value = it.data.sortedBy { element -> element.orderId }
            }
            is Resource.Error -> displayedChild.value = ERROR
            is Resource.Loading -> displayedChild.value = LOADING
        }
    }

}
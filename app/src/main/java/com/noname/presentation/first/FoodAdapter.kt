package com.noname.presentation.first

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Shader
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.ImageViewTarget
import com.noname.R
import com.noname.databinding.ItemFoodBinding
import com.noname.domain.models.Food


class FoodAdapter(private val onItemClicked: (String) -> Unit) :
    ListAdapter<Food, FoodAdapter.FoodViewHolder>(itemCallback) {

    companion object {
        private val itemCallback = object : DiffUtil.ItemCallback<Food>() {

            override fun areItemsTheSame(oldItem: Food, newItem: Food): Boolean {
                return oldItem.orderId == newItem.orderId
            }

            override fun areContentsTheSame(oldItem: Food, newItem: Food): Boolean {
                return oldItem == newItem
            }
        }
        const val GLIDE_TIMEOUT = 60000
        const val GLIDE_THUMBNAIL_WIDTH = 50
        const val GLIDE_THUMBNAIL_HEIGHT = 100
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemFoodBinding.inflate(inflater, parent, false)
        return FoodViewHolder(binding)
    }

    override fun onBindViewHolder(holder: FoodViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class FoodViewHolder(
        private val binding: ItemFoodBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        private val resources: Resources = binding.root.resources

        init {
            binding.root.setOnClickListener {
                binding.model?.url?.let {
                    onItemClicked.invoke(it)
                }
            }
        }

        fun bind(data: Food) {
            binding.model = data
            val placeholder =
                BitmapFactory.decodeResource(
                    resources,
                    R.drawable.placeholder
                )

            Glide.with(binding.foodImage)
                .asBitmap()
                .load(data.imageUrl)
                .timeout(GLIDE_TIMEOUT)
                .apply(RequestOptions().override(GLIDE_THUMBNAIL_WIDTH, GLIDE_THUMBNAIL_HEIGHT))
                .into(object : ImageViewTarget<Bitmap>(binding.foodImage) {
                    override fun onLoadFailed(errorDrawable: Drawable?) {
                        super.onLoadFailed(errorDrawable)
                        setBitmapTiles(placeholder)
                    }

                    override fun onLoadStarted(placeholderDrawable: Drawable?) {
                        super.onLoadStarted(placeholderDrawable)
                        setBitmapTiles(placeholder)
                    }

                    override fun setResource(resource: Bitmap?) {
                        setBitmapTiles(resource)
                    }
                })

        }

        private fun setBitmapTiles(resource: Bitmap?) {
            val bitmapDrawable = BitmapDrawable(resources, resource)
            bitmapDrawable.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT)
            binding.foodImage.background = bitmapDrawable
        }
    }
}
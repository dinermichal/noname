package com.noname.presentation.first

import androidx.fragment.app.Fragment
import com.noname.R
import com.noname.core.navigation.BaseFragmentNavigator
import javax.inject.Inject

class FirstNavigator @Inject constructor(
    fragment: Fragment
) : BaseFragmentNavigator(fragment) {

    fun navigateToSecondFragment() {
        navigate(R.id.action_firstFragment_to_secondFragment)
    }
}
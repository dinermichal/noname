package com.noname.presentation.first

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.navGraphViewModels
import com.noname.R
import com.noname.core.ui.BaseFragment
import com.noname.databinding.FragmentFirstBinding
import com.noname.di.first.FirstInjector
import com.noname.presentation.main.MainViewModel
import javax.inject.Inject

class FirstFragment : BaseFragment<FragmentFirstBinding>() {

    override val layoutId: Int = R.layout.fragment_first

    private val injector: FirstInjector
        get() = baseActivity.application as FirstInjector

    @Inject
    lateinit var navigator: FirstNavigator

    @Inject
    lateinit var viewModelProviderFactory: ViewModelProvider.Factory

    private val isTablet
        get() = requireContext().resources.getBoolean(R.bool.isTablet)

    private val firstViewModel: FirstViewModel by viewModels { viewModelProviderFactory }
    private val mainViewModel: MainViewModel by navGraphViewModels(R.id.nav_graph_main) { viewModelProviderFactory }

    private val adapter: FoodAdapter by lazy { FoodAdapter(::onItemClicked) }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        injector.inject(this)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.viewModel = firstViewModel
        setupView()
    }

    private fun setupView() {
        setupSwipeRefresh()
        setupRecyclerView()
        setupLiveDataObservers()
    }

    private fun setupSwipeRefresh() {
        binding.flipperInclude.listInclude.swipeRefreshLayout.setOnRefreshListener {
            firstViewModel.getFoodList()
        }
    }

    private fun setupRecyclerView() {
        binding.flipperInclude.listInclude.recyclerView.adapter = adapter
    }

    private fun setupLiveDataObservers() {
        firstViewModel.food.observe(viewLifecycleOwner) {
            adapter.submitList(it)
        }
        if (!isTablet) mainViewModel.onUrlSetEvent.observe(viewLifecycleOwner) {
            it?.unhandledData?.let {
                navigator.navigateToSecondFragment()
            }
        }
    }

    override fun onDestroyView() {
        binding.flipperInclude.listInclude.recyclerView.adapter = null
        super.onDestroyView()
    }

    private fun onItemClicked(url: String) = mainViewModel.setUrl(url)

}
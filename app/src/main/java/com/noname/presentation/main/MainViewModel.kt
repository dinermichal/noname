package com.noname.presentation.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.noname.domain.models.Resource
import javax.inject.Inject

class MainViewModel @Inject constructor() : ViewModel() {

    private val _url = MutableLiveData<String>()
    val url: LiveData<String> = _url

    val onUrlSetEvent: LiveData<Resource.Success<Unit>> = Transformations.map(url) {
        Resource.Success(Unit)
    }

    fun setUrl(url: String) {
        _url.value = url
    }
}
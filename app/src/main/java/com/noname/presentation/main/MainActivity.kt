package com.noname.presentation.main

import android.os.Bundle
import com.noname.R
import com.noname.core.ui.BaseActivity
import com.noname.databinding.ActivityMainBinding
import com.noname.di.main.MainInjector

class MainActivity : BaseActivity<ActivityMainBinding>() {

    private val injector: MainInjector
        get() = application as MainInjector

    override val layoutId: Int = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        injector.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onDestroy() {
        injector.clearMainComponent()
        super.onDestroy()
    }
}
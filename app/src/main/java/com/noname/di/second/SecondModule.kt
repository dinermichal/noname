package com.noname.di.second

import androidx.lifecycle.ViewModel
import com.noname.core.viewmodels.ViewModelKey
import com.noname.presentation.second.SecondViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface SecondModule {

    @Binds
    @IntoMap
    @ViewModelKey(SecondViewModel::class)
    fun bindViewModel(viewModel: SecondViewModel): ViewModel
}
package com.noname.di.second

import androidx.fragment.app.Fragment
import com.noname.di.FragmentScope
import com.noname.presentation.second.SecondFragment
import dagger.BindsInstance
import dagger.Subcomponent


@FragmentScope
@Subcomponent(modules = [SecondModule::class])
interface SecondComponent {

    fun inject(fragment: SecondFragment)

    @Subcomponent.Factory
    interface Factory {

        fun bindFragment(@BindsInstance fragment: Fragment): SecondComponent
    }
}
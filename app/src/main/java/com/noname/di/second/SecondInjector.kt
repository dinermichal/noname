package com.noname.di.second

import com.noname.presentation.second.SecondFragment

interface SecondInjector {
    fun inject(fragment: SecondFragment)
}
package com.noname.di

import android.app.Application
import com.noname.di.first.FirstInjector
import com.noname.di.main.MainComponent
import com.noname.di.main.MainInjector
import com.noname.di.second.SecondInjector
import com.noname.di.splash.SplashComponent
import com.noname.di.splash.SplashInjector
import com.noname.presentation.first.FirstFragment
import com.noname.presentation.main.MainActivity
import com.noname.presentation.second.SecondFragment
import com.noname.presentation.splash.SplashActivity

open class InjectorApplication : Application(),
    MainInjector,
    FirstInjector,
    SecondInjector,
    SplashInjector {

    private lateinit var applicationComponent: ApplicationComponent

    private var mainComponent: MainComponent? = null
    private var splashComponent: SplashComponent? = null

    override fun onCreate() {
        super.onCreate()
        initInjector()
    }

    private fun initInjector() {
        DaggerApplicationComponent.factory()
            .bindApplication(this)
            .also { applicationComponent = it }
    }

    override fun inject(activity: MainActivity) {
        mainComponent ?: applicationComponent.getMainComponentFactory()
            .bindActivity(activity)
            .also { mainComponent = it }

        mainComponent?.inject(activity)
    }

    override fun clearMainComponent() {
        mainComponent = null
    }

    override fun inject(fragment: FirstFragment) {
        mainComponent?.firstComponentFactory()
            ?.bindFragment(fragment)
            ?.inject(fragment) ?: throw IllegalStateException()
    }

    override fun inject(fragment: SecondFragment) {
        mainComponent?.secondComponentFactory()
            ?.bindFragment(fragment)
            ?.inject(fragment) ?: throw IllegalStateException()
    }

    override fun inject(activity: SplashActivity) {
        splashComponent ?: applicationComponent.getSplashComponentFactory()
            .bindActivity(activity)
            .also { splashComponent = it }

        splashComponent?.inject(activity)
    }

    override fun clearSplashComponent() {
        splashComponent = null
    }
}
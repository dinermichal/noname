package com.noname.di.splash

import com.noname.presentation.splash.SplashActivity

interface SplashInjector {
    fun inject(activity: SplashActivity)
    fun clearSplashComponent()

}
package com.noname.di.splash

import android.app.Activity
import com.noname.di.FragmentScope
import com.noname.presentation.splash.SplashActivity
import dagger.BindsInstance
import dagger.Subcomponent

@FragmentScope
@Subcomponent()
interface SplashComponent {

    fun inject(activity: SplashActivity)

    @Subcomponent.Factory
    interface Factory {

        fun bindActivity(@BindsInstance activity: Activity): SplashComponent
    }
}
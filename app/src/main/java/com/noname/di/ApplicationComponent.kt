package com.noname.di

import android.app.Application
import com.noname.core.di.CoreModule
import com.noname.data.di.DataModule
import com.noname.di.main.MainComponent
import com.noname.di.splash.SplashComponent
import com.noname.domain.di.DomainModule
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ApplicationModule::class,
        CoreModule::class,
        DataModule::class,
        DomainModule::class
    ]
)
interface ApplicationComponent {

    fun getMainComponentFactory(): MainComponent.Factory
    fun getSplashComponentFactory(): SplashComponent.Factory

    @Component.Factory
    interface Factory {

        fun bindApplication(@BindsInstance application: Application): ApplicationComponent
    }
}
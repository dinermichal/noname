package com.noname.di.first

import com.noname.presentation.first.FirstFragment


interface FirstInjector {

    fun inject(fragment: FirstFragment)
}
package com.noname.di.main

import android.app.Activity
import com.noname.di.ActivityScope
import com.noname.di.first.FirstComponent
import com.noname.di.second.SecondComponent
import com.noname.presentation.main.MainActivity
import dagger.BindsInstance
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [MainModule::class])
interface MainComponent {

    fun inject(activity: MainActivity)
    fun firstComponentFactory(): FirstComponent.Factory
    fun secondComponentFactory(): SecondComponent.Factory

    @Subcomponent.Factory
    interface Factory {

        fun bindActivity(@BindsInstance activity: Activity): MainComponent
    }
}
package com.noname.di.main

import com.noname.presentation.main.MainActivity

interface MainInjector {

    fun inject(activity: MainActivity)
    fun clearMainComponent()
}
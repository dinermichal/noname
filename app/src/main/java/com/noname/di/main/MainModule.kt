package com.noname.di.main

import androidx.lifecycle.ViewModel
import com.noname.core.viewmodels.ViewModelKey
import com.noname.presentation.main.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface MainModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    fun bindViewModel(viewModel: MainViewModel): ViewModel
}
package com.noname.core.di

import androidx.lifecycle.ViewModelProvider
import com.noname.core.viewmodels.ViewModelProviderFactory
import dagger.Binds
import dagger.Module


@Module(includes = [CoroutinesModule::class])
interface CoreModule {

    @Binds
    fun bindViewModelProviderFactory(factory: ViewModelProviderFactory): ViewModelProvider.Factory
}
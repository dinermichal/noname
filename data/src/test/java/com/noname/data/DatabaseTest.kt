package com.noname.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.noname.data.database.AppDatabase
import com.noname.data.database.FoodDao
import com.noname.domain.models.Food
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class DatabaseTest {

    @RelaxedMockK
    lateinit var appDatabase: AppDatabase

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    lateinit var dao: FoodDao

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        dao = appDatabase.foodDao()
    }

    @Test
    fun `saving model and retrieving it`() {
        val list = listOf(
            Food(
                1,
                "\t",
                "",
                "1980-08-03",
                5,
                ""
            )
        )
        coEvery {
            dao.insertFoodList(list)
        } coAnswers {
            val cachedList = dao.loadAllFood()
            assertEquals(list, cachedList)
        }
    }

    @Test
    fun `updating model and retrieving it`() {
        val list = listOf(
            Food(
                1,
                "\t",
                "",
                "1980-08-03",
                5,
                ""
            )
        )
        coEvery {
            dao.updateFood(list)
        } coAnswers {
            val cachedList = dao.loadAllFood()
            assertEquals(list, cachedList)
        }
    }
}
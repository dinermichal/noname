package com.noname.data.services

import com.noname.data.networking.models.FoodApiModel

interface FoodServiceApi {
    suspend fun getFood(): List<FoodApiModel>
}
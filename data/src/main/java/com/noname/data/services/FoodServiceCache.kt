package com.noname.data.services

import com.noname.domain.models.Food

interface FoodServiceCache {
    suspend fun getFood(): List<Food>
    suspend fun updateFood(list: List<Food>)
}
package com.noname.data.services.database

import com.noname.data.database.AppDatabase
import com.noname.data.services.FoodServiceCache
import com.noname.domain.models.Food
import javax.inject.Inject

class FoodServiceCacheImpl @Inject constructor(appDatabase: AppDatabase) : FoodServiceCache {
    private val dao = appDatabase.foodDao()
    override suspend fun getFood(): List<Food> = dao.loadAllFood()
    override suspend fun updateFood(list: List<Food>) = dao.updateFood(list)
}
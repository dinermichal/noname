package com.noname.data.services.base

import com.noname.data.networking.models.wrappers.ApiException

abstract class ApiService {

    suspend fun <T> request(apiCall: suspend () -> T): T {
        return try {
            apiCall.invoke()
        } catch (throwable: Throwable) {
            throw ApiException.asRestException(throwable)
        }
    }
}
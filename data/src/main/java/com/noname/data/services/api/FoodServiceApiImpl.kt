package com.noname.data.services.api

import com.noname.data.networking.endpoints.FoodEndpoint
import com.noname.data.networking.models.FoodApiModel
import com.noname.data.services.FoodServiceApi
import com.noname.data.services.base.ApiService
import javax.inject.Inject

class FoodServiceApiImpl @Inject constructor(
    private val endpoint: FoodEndpoint
) : ApiService(), FoodServiceApi {

    override suspend fun getFood(): List<FoodApiModel> = request {
        endpoint.getFood()
    }
}
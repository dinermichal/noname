package com.noname.data.mappers

import com.noname.data.networking.models.FoodApiModel
import com.noname.domain.models.Food

fun FoodApiModel.toDomain() = Food(
    content = content,
    imageUrl = imageUrL,
    modificationDate = modificationDate,
    orderId = orderId,
    title = title
)
package com.noname.data.repositories

import com.noname.data.mappers.toDomain
import com.noname.data.services.FoodServiceApi
import com.noname.data.services.FoodServiceCache
import com.noname.domain.models.Food
import com.noname.domain.repositories.FoodRepository
import javax.inject.Inject

class FoodRepositoryImpl @Inject constructor(
    private val foodServiceApi: FoodServiceApi,
    private val foodServiceCache: FoodServiceCache,
) : FoodRepository {

    override suspend fun getFood(): List<Food> {
        return runCatching { foodServiceApi.getFood() }
            .map { list ->
                list.map {
                    it.toDomain()
                }
            }.map {
                foodServiceCache.updateFood(it)
                it
            }
            .recover {
                val cached = foodServiceCache.getFood()
                if (cached.isNotEmpty()) {
                    cached
                } else {
                    throw IllegalStateException()
                }
            }.getOrThrow()
    }
}
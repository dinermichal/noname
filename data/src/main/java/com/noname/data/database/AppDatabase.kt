package com.noname.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.noname.domain.models.Food

@Database(entities = [Food::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun foodDao(): FoodDao
}

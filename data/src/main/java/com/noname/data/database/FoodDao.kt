package com.noname.data.database

import androidx.room.*
import com.noname.domain.models.Food

@Dao
abstract class FoodDao {
    @Transaction
    open suspend fun updateFood(foodList: List<Food>) {
        deleteFood()
        insertFoodList(foodList)
    }

    @Query("DELETE FROM food")
    abstract suspend fun deleteFood()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertFoodList(food: List<Food>)

    @Query("SELECT * FROM food")
    abstract suspend fun loadAllFood(): List<Food>
}
package com.noname.data.networking.endpoints

import com.noname.data.networking.models.FoodApiModel
import retrofit2.http.GET

interface FoodEndpoint {

    @GET("recruitment-task")
    suspend fun getFood(): List<FoodApiModel>
}
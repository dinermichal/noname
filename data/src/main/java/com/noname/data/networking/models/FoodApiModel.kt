package com.noname.data.networking.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FoodApiModel(
    @Json(name = "description")
    val content: String,
    @Json(name = "image_url")
    val imageUrL: String,
    val modificationDate: String,
    val orderId: Long,
    val title: String
)
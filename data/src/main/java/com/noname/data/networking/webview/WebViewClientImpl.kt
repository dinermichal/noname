package com.noname.data.networking.webview

import android.graphics.Bitmap
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import com.noname.domain.models.WebViewClient
import com.noname.domain.models.WebViewClientState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject
import android.webkit.WebViewClient as AndroidWebViewClient

class WebViewClientImpl @Inject constructor() : AndroidWebViewClient(), WebViewClient {
    private val _clientState: MutableStateFlow<WebViewClientState> =
        MutableStateFlow(WebViewClientState.OnPageStarted)
    override val clientState: StateFlow<WebViewClientState> = _clientState

    override fun shouldOverrideUrlLoading(
        view: WebView?,
        request: WebResourceRequest?
    ): Boolean {
        return false
    }

    override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
        super.onPageStarted(view, url, favicon)
        _clientState.value = WebViewClientState.OnPageStarted
    }

    override fun onPageFinished(view: WebView?, url: String?) {
        super.onPageFinished(view, url)
        _clientState.value = WebViewClientState.OnPageFinished
    }

    override fun onReceivedError(
        view: WebView?,
        request: WebResourceRequest?,
        error: WebResourceError
    ) {
        super.onReceivedError(view, request, error)
        _clientState.value = WebViewClientState.OnReceivedError(error.description)
    }

}
package com.noname.data.networking.webview

import android.webkit.WebView
import com.noname.domain.models.WebChromeClient
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject
import android.webkit.WebChromeClient as AndroidWebChromeClient

class WebChromeClientImpl @Inject constructor() : AndroidWebChromeClient(), WebChromeClient {
    private val _progressState: MutableStateFlow<Int> =
        MutableStateFlow(0)
    override val progressState: StateFlow<Int> = _progressState

    override fun onProgressChanged(view: WebView?, newProgress: Int) {
        super.onProgressChanged(view, newProgress)
        _progressState.value = newProgress
    }
}
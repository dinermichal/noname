package com.noname.data.di

import dagger.Module

@Module(
    includes = [
        RetrofitModule::class,
        RepositoriesModule::class,
        ServicesModule::class,
        EndpointsModule::class,
        DatabaseModule::class,
        WebViewModule::class]
)
interface DataModule
package com.noname.data.di

import com.noname.data.repositories.FoodRepositoryImpl
import com.noname.domain.repositories.FoodRepository
import dagger.Binds
import dagger.Module

@Module
internal interface RepositoriesModule {

    @Binds
    fun bindFoodRepository(repository: FoodRepositoryImpl): FoodRepository
}
package com.noname.data.di

import com.noname.data.services.FoodServiceApi
import com.noname.data.services.FoodServiceCache
import com.noname.data.services.api.FoodServiceApiImpl
import com.noname.data.services.database.FoodServiceCacheImpl
import dagger.Binds
import dagger.Module

@Module
internal interface ServicesModule {

    @Binds
    fun bindFoodServiceApi(service: FoodServiceApiImpl): FoodServiceApi

    @Binds
    fun bindFoodServiceCache(service: FoodServiceCacheImpl): FoodServiceCache
}
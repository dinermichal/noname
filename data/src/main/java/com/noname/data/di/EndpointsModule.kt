package com.noname.data.di

import com.noname.data.networking.endpoints.FoodEndpoint
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
object EndpointsModule {

    @Provides
    fun getFoodEndpoint(retrofit: Retrofit): FoodEndpoint {
        return retrofit.create(FoodEndpoint::class.java)
    }
}
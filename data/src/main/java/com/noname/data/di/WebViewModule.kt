package com.noname.data.di

import com.noname.data.networking.webview.WebChromeClientImpl
import com.noname.data.networking.webview.WebViewClientImpl
import com.noname.domain.models.WebChromeClient
import com.noname.domain.models.WebViewClient
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface WebViewModule {

    @Binds
    @Singleton
    fun bindWebViewClient(webViewClientImpl: WebViewClientImpl): WebViewClient

    @Binds
    @Singleton
    fun bindWebChromeClient(webChromeClient: WebChromeClientImpl): WebChromeClient
}
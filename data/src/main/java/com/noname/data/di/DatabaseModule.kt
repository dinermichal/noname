package com.noname.data.di

import android.content.Context
import androidx.room.Room
import com.noname.data.database.AppDatabase
import dagger.Module
import dagger.Provides

@Module
class DatabaseModule {

    companion object {
        const val DATABASE_NAME = "database-name"
    }

    @Provides
    fun provideDatabase(context: Context): AppDatabase =
        Room.databaseBuilder(
            context,
            AppDatabase::class.java, DATABASE_NAME
        ).build()

}